Missing sources for bambootracker
=================================

This directory contains sources (a git submodule) that are missing from upstream's tarball.
Downloaded from https://github.com/rerrahkr/emu2149/tree/63c0fe07df89bebdaccbfe982a3f737b39b409d3

emu2149
=======

A YM2149 (aka PSG) emulator written in C.
